package u04lab.solutions

import u04lab.code.Complex

/**
  * Created by nicola.santolini on 21/03/2017.
  */
case class ComplexImpl(override val re: Double,
                  override val im: Double) extends Complex{

  override def +(c: Complex): Complex = Complex(this.re + c.re, this.im + c.im)

  override def *(c: Complex): Complex = Complex(this.re * c.re + this.im * c.im * -1, this.re * c.im + this.im * c.re)

  override def toString: String = "ComplexImpl(" +re + "," + im + ")"

}
